﻿using ACM.BL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace ACM.BLTest
{
    [TestClass]
    public class CustomerRepositoryTest
    {
        [TestMethod]
        public void RetrieveValid()
        {
            //-- Arrange
            var customerRepository = new CustomerRepository();
            var expected = new Customer(1)
            {
                EmailAddress = "fbaggins@hobbiton.me",
                FirstName = "Frodo",
                LastName = "Baggins"
            };

            //-- Act
            var actual = customerRepository.Retrieve(1);

            //-- Assert
            Assert.AreEqual(expected.CustomerId, actual.CustomerId);
            Assert.AreEqual(expected.EmailAddress, actual.EmailAddress);
            Assert.AreEqual(expected.FirstName, actual.FirstName);
            Assert.AreEqual(expected.LastName, actual.LastName);
        }

        [TestMethod]
        public void RetrieveExistingWithAddress()
        {
            //-- Arrange
            var customerRepository = new CustomerRepository();
            var expected = new Customer(1)
            {
                EmailAddress = "fbaggins@hobbiton.me",
                FirstName = "Frodo",
                LastName = "Baggins",
                AddresssList = new List<Address>()
                             {
                                new Address ()
                                {
                                    AddressType = 1,
                                    StreetLine1 = "Bag End",
                                    StreetLine2 = "Bagshort Row",
                                    City = "Hobbiton",
                                    State = "Shire",
                                    Country = "Middle Earth",
                                    PostalCode = "144"
                                },
        
                                new Address()
                                {
                                    AddressType = 2,
                                    StreetLine1 = "Green Dragon",
                                    City = "Bywater",
                                    State = "Shire",
                                    Country = "Middle Earth",
                                    PostalCode = "146"
                                }
                             }
            };

            //-- Act
            var actual = customerRepository.Retrieve(1);

            //-- Assert
            Assert.AreEqual(expected.CustomerId, actual.CustomerId);
            Assert.AreEqual(expected.EmailAddress, actual.EmailAddress);
            Assert.AreEqual(expected.FirstName, actual.FirstName);
            Assert.AreEqual(expected.LastName, actual.LastName);

            for (int i = 0; i < 1; i++)
            {
                Assert.AreEqual(expected.AddresssList[i].AddressType, actual.AddresssList[i].AddressType);
                Assert.AreEqual(expected.AddresssList[i].StreetLine1, actual.AddresssList[i].StreetLine1);
                Assert.AreEqual(expected.AddresssList[i].City, actual.AddresssList[i].City);
                Assert.AreEqual(expected.AddresssList[i].State, actual.AddresssList[i].State);
                Assert.AreEqual(expected.AddresssList[i].Country, actual.AddresssList[i].Country);
                Assert.AreEqual(expected.AddresssList[i].PostalCode, actual.AddresssList[i].PostalCode);
            }
        }
    }
}
